/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app;

import android.test.ActivityInstrumentationTestCase2;

import uk.co.smcnamee.elreg.app.activities.ForumActivity;

/**
 * Created by a543097 on 01/05/2015.
 */
public class ForumActivityTest extends ActivityInstrumentationTestCase2<ForumActivity> {

    public ForumActivityTest() {
        super(ForumActivity.class);
    }
}
