package uk.co.smcnamee.elreg.app;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.FrameLayout;

import uk.co.smcnamee.elreg.app.activities.MainActivity;

/**
 * Created by a543097 on 20/04/2015.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity>{

    MainActivity mainActivity;
    FrameLayout layoutContainer;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception{
        super.setUp();
        mainActivity = getActivity();
        layoutContainer = (FrameLayout) mainActivity.findViewById(R.id.container);
        //TODO grab any UI components here
    }

    @SmallTest
    public void testPreconditions(){
        assertNotNull("MainActivity is null", mainActivity);
        assertNotNull("Container is null", layoutContainer);

    }



}
