/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.FrameLayout;

import uk.co.smcnamee.elreg.app.activities.MainActivity;

/**
 * Created by a543097 on 20/04/2015.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity>{

    MainActivity mainActivity;
    FrameLayout layoutContainer;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception{
        super.setUp();
        mainActivity = getActivity();
        layoutContainer = (FrameLayout) mainActivity.findViewById(R.id.container);
        //TODO grab any UI components here
    }

    @SmallTest
    public void testPreconditions(){
        assertNotNull("MainActivity is null", mainActivity);
        assertNotNull("Container is null", layoutContainer);

    }



}
