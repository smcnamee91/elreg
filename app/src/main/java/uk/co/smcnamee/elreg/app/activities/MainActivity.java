/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import elreg.smcnamee.co.uk.secureapi.model.Story;
import elreg.smcnamee.co.uk.secureapi.wrapper.SecureApi;
import io.fabric.sdk.android.Fabric;
import uk.co.smcnamee.elreg.app.R;
import uk.co.smcnamee.elreg.app.settings.SettingsActivity;
import uk.co.smcnamee.elreg.app.utils.adapters.StoryAdapter;


public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, SecureApi.ApiSectionCallbacks, SharedPreferences.OnSharedPreferenceChangeListener{

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "j8LorkwFkMiF9IQo7DXEYzCIE";
    private static final String TWITTER_SECRET = "CInGiMAo63y1nG7SNGtYXn1QKp0aoCUw8JFl1mkhgg7TNhTtb1";


    public static Story lastStoryClicked;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    protected NavigationDrawerFragment mNavigationDrawerFragment;
    Toolbar mToolbar;
    SwipeRefreshLayout srl;
    int currentSelection;
    private RecyclerView recyclerView;
    private SecureApi elRegApi;
    private ArrayList<Story> stories = new ArrayList<>(0);
    private HashMap<String,List<Story>> sections = new HashMap<>();

    float title_size;
    float subhead_size;
    float caption_size;

    private int NUMBER_PER_SECTION;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private DrawerLayout mDrawerLayout;
    private float lastScaleValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);

        NUMBER_PER_SECTION = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this)
                .getString("number_to_sync", "10"));

        elRegApi = SecureApi.getInstance(this,
                getResources().getString(R.string.api_username),
                getResources().getString(R.string.api_key));

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mTitle = getTitle();

        mToolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(mToolbar);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new StoryAdapter(stories));

        srl = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isConnectedToNetwork()){
                    elRegApi.getSectionStories(NUMBER_PER_SECTION, getResources().getStringArray(R.array.feeds)[currentSelection]);
                    //elRegApi.getAllArticles(NUMBER_PER_SECTION, getResources().getStringArray(R.array.json_sections));
                } else{
                    srl.setRefreshing(false);
                    Toast.makeText(MainActivity.this,"Failed to load articles, check internet connection and try again", Toast.LENGTH_SHORT).show();
                }
            }
        });

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                return getSavedFeeds();
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                if(aBoolean && !isConnectedToNetwork()){
                    onNavigationDrawerItemSelected(0);
                }else if(aBoolean && isConnectedToNetwork()){
                    onNavigationDrawerItemSelected(0);
                    elRegApi.getSectionStories(NUMBER_PER_SECTION, getResources().getStringArray(R.array.feeds)[0]);
                    getAllSections();
                } else if (!aBoolean && isConnectedToNetwork()){
                    elRegApi.getSectionStories(NUMBER_PER_SECTION, getResources().getStringArray(R.array.feeds)[0]);
                    getAllSections();
                } else{
                    // TODO no network.. show error and offer retry
                }
            }
        }.execute();

        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);


        title_size = getResources().getDimension(R.dimen.abc_text_size_title_material); //(TextView) findViewById(R.id.story_title)).getTextSize();
        subhead_size = getResources().getDimension(R.dimen.abc_text_size_subhead_material);
        caption_size = getResources().getDimension(R.dimen.abc_text_size_caption_material);

        scaleFonts(this, findViewById(R.id.container),
                PreferenceManager.getDefaultSharedPreferences(this).getFloat("basic_slider",1));
    }

    private void getAllSections() {
        elRegApi.getAllSectionStories(NUMBER_PER_SECTION, getResources().getStringArray(R.array.json_sections));
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveFeeds();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        mTitle = getResources().getStringArray(R.array.categories)[position];

        if(sections != null && sections.containsKey(getResources().getStringArray(R.array.json_sections)[position])){
            setArticles(sections.get(getResources().getStringArray(R.array.json_sections)[position]));
        }else{
            elRegApi.getSectionStories(NUMBER_PER_SECTION, getResources().getStringArray(R.array.feeds)[position]);
        }

        currentSelection = position;

        getSupportActionBar().setTitle(mTitle);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.menu_forum, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent i;

        switch (id){
            case R.id.action_about:
                 i = new Intent(this, AboutActivity.class);
                startActivity(i);
                break;
            case R.id.action_settings:
                i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startArtActivity(View v){
        if(isSwipeEnabled()){
            Intent i = new Intent(this, ArticleSwipeActivity.class);
            i.putParcelableArrayListExtra("STORIES",stories);
            View vt = v.findViewById(R.id.story_title);
            i.putExtra("STORYNUM", Integer.parseInt(vt.getTag().toString()));
            startActivity(i);
        }else{
            Intent intent = new Intent(this, ArticleActivity.class);
            View vt = v.findViewById(R.id.story_title);
            intent.putExtra("STORY",(Parcelable)stories.get(Integer.parseInt(vt.getTag().toString())));
            startActivity(intent);
        }

    }

    private boolean isSwipeEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(this).getBoolean("swipe_enabled",false);
    }


    public void setArticles(List<Story> stories) {
        this.stories.clear();
        this.stories.addAll(stories);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(0);
    }

    private void saveFeeds(){
        try
        {
            FileOutputStream fileOut = new FileOutputStream(getCacheDir().getAbsolutePath() + "/feeds.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(sections);
            out.close();
            fileOut.close();
        }catch(IOException i)
        {
            i.printStackTrace();
        }
    }

    private boolean getSavedFeeds(){
        File feedDir = new File(getCacheDir().getAbsolutePath());
        boolean found = false;

        if(feedDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.equals("feeds.ser");
            }
        }).length == 1){
            FileInputStream fileIn = null;
            ObjectInputStream in = null;
            try{
                fileIn = new FileInputStream(getCacheDir().getAbsolutePath() + "/feeds.ser");
                in = new ObjectInputStream(fileIn);
                sections = (HashMap<String, List<Story>>) in.readObject();
                found = true;
                in.close();
                fileIn.close();
            } catch(EOFException eof){
                found = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return found;
    }

    @Override
    public void onSectionLoaded(String key, List<Story> section) {
//        if(sections == null){
//            sections = new HashMap<>();
//        }
        sections.put(getResources().getStringArray(R.array.json_sections)[currentSelection], section);
        stories.clear();
        stories.addAll(sections.get(getResources().getStringArray(R.array.json_sections)[currentSelection]));
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.smoothScrollToPosition(0);
        if(srl.isRefreshing()){
            srl.setRefreshing(false);
        }
    }

    @Override
    public void onGetAllSectionsComplete(HashMap<String, List<Story>> sections) {
        this.sections = sections;
    }

    private boolean isConnectedToNetwork(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return  (activeNetwork != null) && (activeNetwork.isConnectedOrConnecting());
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("number_to_sync")){
            NUMBER_PER_SECTION = Integer.parseInt(sharedPreferences.getString(key,"10"));
            if(isConnectedToNetwork()){
                elRegApi.getSectionStories(NUMBER_PER_SECTION, getResources().getStringArray(R.array.feeds)[currentSelection]);
            }
        }
        if(key.equals("basic_slider")){
            scaleFonts(this, findViewById(R.id.container), sharedPreferences.getFloat(key,1));
            recyclerView.removeAllViews();
            recyclerView.getAdapter().onAttachedToRecyclerView(recyclerView);
        }
    }

    private void scaleFonts(final Context context, final View v, float scale) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    scaleFonts(context, child, scale);
                }
            } else if (v instanceof TextView) {
                switch(v.getId()){
                    case R.id.story_title:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, title_size * (scale));
                        break;
                    case R.id.story_standfirst:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, subhead_size * (scale));
                        break;
                    case R.id.story_time:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, caption_size * (scale));
                        break;
                }
            }
        } catch (Exception e) {
        }
    }
}
