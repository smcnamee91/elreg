/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */
package uk.co.smcnamee.elreg.app.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.CompactTweetView;
import com.twitter.sdk.android.tweetui.LoadCallback;
import com.twitter.sdk.android.tweetui.TweetUtils;

import uk.co.smcnamee.elreg.app.activities.ArticleActivity;

/**
 * Created by Simon on 22/06/2015.
 */
public class TweetSpan implements LoadCallback<Tweet> {

    URLDrawable tweetImage;
    Context context;
    int startSpan;
    int endSpan;

    public TweetSpan(Context context, int startSpan, int endSpan){
        this.context = context;
        this.startSpan = startSpan;
        this.endSpan = endSpan;
    }

    public Drawable getTweetImage(long tweetID){

        TweetUtils.loadTweet(tweetID, this);
        tweetImage = new URLDrawable();

        return this.tweetImage;
    }

    public Bitmap loadBitmapFromView(View view) {
        //Pre-measure the view so that height and width don't remain null.
        view.measure(CompactTweetView.MeasureSpec.makeMeasureSpec((int)ArticleActivity.ARTICLE_WIDTH, CompactTweetView.MeasureSpec.EXACTLY),
                CompactTweetView.MeasureSpec.makeMeasureSpec(0, CompactTweetView.MeasureSpec.UNSPECIFIED));

        //Assign a size and position to the view and all of its descendants
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        //Create the bitmap
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmap);

        //Render this view (and all of its children) to the given Canvas
        view.draw(c);

        return bitmap;
    }

    @Override
    public void success(Tweet tweet) {
        CompactTweetView v = new CompactTweetView(context, tweet);

        this.tweetImage.setDrawable(new BitmapDrawable(loadBitmapFromView(v)));

//        float multiplier = ArticleActivity.ARTICLE_WIDTH / (float)tweetImage.getIntrinsicWidth();
//
//        int width = (int)( this.tweetImage.getIntrinsicWidth() * multiplier);
//        int height = (int)( this.tweetImage.getIntrinsicHeight() * multiplier);
//
//        this.tweetImage.setBounds(0, 0, width, height);

        ((ArticleActivity)context).updateSpan(tweetImage, startSpan, endSpan);
    }

    @Override
    public void failure(TwitterException e) {
        Log.e("TwitterE", "oops", e);
    }

}
