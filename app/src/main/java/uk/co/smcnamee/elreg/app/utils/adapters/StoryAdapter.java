/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app.utils.adapters;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import elreg.smcnamee.co.uk.secureapi.model.Story;
import uk.co.smcnamee.elreg.app.R;

/**
 * Created by Simon on 01/02/2015.
 */
public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ViewHolder> {

    private List<Story> stories;


    public StoryAdapter(List<Story> stories) {
        this.stories = stories;

    }

    @Override
    public StoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {

        //create view and viewholder
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_story_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        
        return viewHolder;
    }

    // Replace the contents of a view
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.txtViewTitle.setText(stories.get(position).getHeadline());
        viewHolder.txtViewTitle.setTag(position);
        viewHolder.txtViewStandfirst.setText(stories.get(position).getStandfirst());
        viewHolder.txtViewTime.setText(stories.get(position).getTimeSincePost());

    }

    // Returns the size of the fruitsData
    @Override
    public int getItemCount() {
        return stories.size();
    }



    // class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtViewTitle;
        public TextView txtViewStandfirst;
        public TextView txtViewTime;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.story_title);
            txtViewStandfirst = (TextView) itemLayoutView.findViewById(R.id.story_standfirst);
            txtViewTime = (TextView) itemLayoutView.findViewById(R.id.story_time);

            scaleFonts(itemLayoutView.getContext(), itemLayoutView,
                    PreferenceManager.getDefaultSharedPreferences(itemLayoutView.getContext()).getFloat("basic_slider", 1));

        }

        private void scaleFonts(final Context context, final View v, float scale) {
            try {
                if (v instanceof ViewGroup) {
                    ViewGroup vg = (ViewGroup) v;
                    for (int i = 0; i < vg.getChildCount(); i++) {
                        View child = vg.getChildAt(i);
                        scaleFonts(context, child, scale);
                    }
                } else if (v instanceof TextView) {
                    ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, ((TextView) v).getTextSize()
                            * (scale));
                }
            } catch (Exception e) {
            }
        }
    }
}
