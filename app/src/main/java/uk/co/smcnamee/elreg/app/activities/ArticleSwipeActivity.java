package uk.co.smcnamee.elreg.app.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import elreg.smcnamee.co.uk.secureapi.model.Story;
import uk.co.smcnamee.elreg.app.R;
import uk.co.smcnamee.elreg.app.fragments.ArticleFragment;
import uk.co.smcnamee.elreg.app.fragments.helpers.ReaderViewPagerTransformer;
import uk.co.smcnamee.elreg.app.fragments.helpers.ZoomOutPageTransformer;
import uk.co.smcnamee.elreg.app.settings.SettingsActivity;
import uk.co.smcnamee.elreg.app.fragments.helpers.ArticlePagerAdapter;

public class ArticleSwipeActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    private Toolbar mToolbar;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private ArrayList<Story> stories;
    private ShareActionProvider mShareActionProvider;
    private int startItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_swipe);

        if(savedInstanceState != null){
            stories = savedInstanceState.getParcelableArrayList("STORIES");
            startItem = savedInstanceState.getInt("STARTITEM");
        }else{
            stories = getIntent().getParcelableArrayListExtra("STORIES");
            startItem = getIntent().getIntExtra("STORYNUM", 0);
        }

        mToolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        int preferedAnimation = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("swipe_animation", "1"));
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ArticlePagerAdapter(getSupportFragmentManager(), stories);
        mPager.setAdapter(mPagerAdapter);
        mPager.setOffscreenPageLimit(1);
        mPager.setPageTransformer(true, new ReaderViewPagerTransformer(preferedAnimation));
        mPager.setCurrentItem(startItem);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(ArticleSwipeActivity.this, (position + 1) + " of " + stories.size(), Toast.LENGTH_SHORT).show();
                setShareIntent();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);


    }

    @Override
    protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        icicle.putParcelableArrayList("STORIES", stories);
        icicle.putInt("STARTITEM",startItem);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_art, menu);

        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(new Intent(Intent.ACTION_SEND));
        setShareIntent();

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent i;

        switch (id){
            case R.id.action_about:
                i = new Intent(this, AboutActivity.class);
                startActivity(i);
                break;
            case R.id.action_settings:
                i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public ShareActionProvider getmShareActionProvider(){
        return this.mShareActionProvider;
    }

    private void setShareIntent(){
        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mPager.getCurrentItem());
        // based on the current position you can then cast the page to the correct
        // class and call the method:
        if (page != null) {
            ((ArticleFragment)page).getShareIntent();
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("swipe_animation")){
            mPager.setPageTransformer(true, new ReaderViewPagerTransformer(Integer.parseInt(sharedPreferences.getString("swipe_animation", "1"))));
        }
    }
}
