/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import elreg.smcnamee.co.uk.secureapi.model.Forum;
import uk.co.smcnamee.elreg.app.R;
import uk.co.smcnamee.elreg.app.settings.SettingsActivity;
import uk.co.smcnamee.elreg.app.utils.adapters.CommentAdapter;


public class ForumActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    Forum forum;
    Toolbar mToolbar;


    float caption_size;
    float body2_size;
    float body1_size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);

        if(savedInstanceState != null){
            forum = savedInstanceState.getParcelable("FORUM");
        }else{
            this.forum = getIntent().getParcelableExtra("FORUM");
        }

        mToolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.comment_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CommentAdapter(forum));

        caption_size = getResources().getDimension(R.dimen.abc_text_size_caption_material);
        body1_size = getResources().getDimension(R.dimen.abc_text_size_body_1_material);
        body2_size = getResources().getDimension(R.dimen.abc_text_size_body_2_material);

        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);

        scaleFonts(this, findViewById(R.id.root_view),
                PreferenceManager.getDefaultSharedPreferences(this).getFloat("basic_slider", 1));

    }

    public void onSaveInstanceState(Bundle icicle){
        super.onSaveInstanceState(icicle);
        icicle.putParcelable("FORUM",forum);
    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStop() {
//        PreferenceManager.getDefaultSharedPreferences(this)
//                .unregisterOnSharedPreferenceChangeListener(this);
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_forum, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent i;

        switch (id){
            case R.id.action_about:
                i = new Intent(this, AboutActivity.class);
                startActivity(i);
                break;
            case R.id.action_settings:
                i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void scaleFonts(final Context context, final View v, float scale) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    scaleFonts(context, child, scale);
                }
            } else if (v instanceof TextView) {
                switch (v.getId()){
                    case R.id.post_handle:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, body2_size * (scale));
                        break;
                    case R.id.post_time:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, caption_size * (scale));
                        break;
                    case R.id.post_title:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, body2_size * (scale));
                        break;
                    case R.id.post_content:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, body1_size * (scale));
                        break;
                }

            }
        } catch (Exception e) {
            Log.e("ScaleFont", e.getMessage());
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("basic_slider")){
            scaleFonts(this, findViewById(R.id.root_view),sharedPreferences.getFloat(key,1));
        }
    }
}
