/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app.activities;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import uk.co.smcnamee.elreg.app.R;

/**
 * Created by Simon on 01/02/2015.
 */
public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    int[] icons = {R.drawable.ic_icon_elreg,
            R.drawable.ic_icon_datacenter,
            R.drawable.ic_icon_software,
            R.drawable.ic_icon_networks,
            R.drawable.ic_icon_security,
            R.drawable.ic_icon_security,
            R.drawable.ic_icon_business,
            R.drawable.ic_icon_hardware,
            R.drawable.ic_icon_science,
            R.drawable.ic_icon_weekend,
            R.drawable.ic_icon_weekend};
    private String[] categories;

    public DrawerAdapter(String[] categories) {
        this.categories = categories;

    }

    @Override
    public DrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {

        //create view and viewholder
        if(viewType == TYPE_HEADER){
            View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.header, parent, false);
            ViewHolder viewHolder = new ViewHolder(itemLayoutView,viewType);


            return viewHolder;
        } else{
            View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,  parent, false);
            ViewHolder viewHolder = new ViewHolder(itemLayoutView,viewType);

            return viewHolder;
        }

    }

    // Replace the contents of a view
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        if(viewHolder.holderType ==1) {                              // as the list view is going to be called after the header view so we decrement the
            viewHolder.txtViewTitle.setText(categories[position-1]);
            viewHolder.imageView.setImageResource(icons[position-1]);
            ((LinearLayout)viewHolder.txtViewTitle.getParent()).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) v.getContext()).mNavigationDrawerFragment.selectItem(position - 1);

                }
            });
        }
        else{

        }
    }

    @Override
    public int getItemCount() {
        return categories.length + 1;
    }

    // Witht the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    // class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtViewTitle;
        public ImageView imageView;
        public int holderType;

        public ViewHolder(View itemLayoutView, int type) {
            super(itemLayoutView);
            if(type == TYPE_HEADER){
                holderType = TYPE_HEADER;
            }else{
                txtViewTitle = (TextView) itemLayoutView.findViewById(R.id.rowText);
                imageView = (ImageView) itemLayoutView.findViewById(R.id.rowIcon);
                holderType = TYPE_ITEM;
            }

        }
    }


}
