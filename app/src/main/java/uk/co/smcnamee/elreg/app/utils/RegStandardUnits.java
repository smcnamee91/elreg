/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by a543097 on 23/07/2015.
 */
public class RegStandardUnits {

    enum Area{
        SQUARE_MILE(1d), ACRE(640), SQUARE_YARD(3098000d), SQUARE_FOOT(27880000d),SQUARE_INCH(4014000000d),
        SQUARE_KILOMETRE(2.59), SQUARE_METRE(2590000d), SQUARE_CENTIMETRE(25900000000d),
        SQUARE_MILIMETRE(2590000000000d), HECTARE(259d), NANO_WALES(1000000000d / 8022d),
        MICRO_WALES(1000000d / 8022d), MILLI_WALES(1000d / 8022d), WALES(1d / 8022d),
        FOOTBALL_PITCH(639.38618), BELGIUM(1d / 11787d), CONGO(1d / 908892.6);

        double value;

        Area(double d){
            value = d;
        }
    }

    enum Length{
        MILE(0.00008699), FURLONG(0.0006959), YARD(0.1531), FOOT(0.4593), INCH(5.512),
        KILOMETRE(0.00014), METRE(0.14), DECIMETRE(1.4), CENTIMETRE(14d), MILLIMETRE(140d),
        LINGUINE(1d), DOUBLE_DECKER_BUS(1d / 65.85), BRONTOSAURUS(1d / 987.751);

        double value;

        Length(double d){
            this.value = d;
        }
    }

    enum Force{
        POUNDALS(723.3), NEWTONS(100d), POUNDS_OF_FORCE(22.481), KILO_OF_FORCE(10.197), KILO_NEWTON(0.1),
        NORRIS(1d);

        double value;

        Force(double d){
            this.value = d;
        }
    }

    enum Volume{
        CUBIC_YARD(0.0006848), CUBIC_FOOT(0.01849), CUBIC_INCH(31.95),
        CUBIC_METRE(0.0005236), CUBIC_CENTIMETRE(523.6), CUBIC_MILLIMETRE(523600),
        WALNUT(1/0.16), CHICKEN_EGG(1/0.35), GRAPEFRUIT(1),
        AIRBAG(1/1.1), FUNBAG(1/3.27),
        FOOTBALL(1/11.07), SWIMMINGPOOL(1/4780114);

        double value;

        Volume(double d){
            this.value = d;
        }
    }

    enum Weight{
        HUNDREDWEIGHT_UK(0.08267), HUNDREDWEIGHT_US(0.09259),
        STONE(0.6614), POUND(9.259), OUNCE(148.2),
        TONNE(0.0042), KILOGRAM(4.2), GRAM(4200), CENTIGRAM(420000), MILLIGRAM(4200000),
        KILOJUB(0.001), JUB(1), MILLIJUB(1000), MICROJUB(1000000);

        double value;

        Weight(double d){
            this.value = d;
        }
    }

    enum Temp{
        FAHRENHEIT, CELSIUS, HILTON;
    }

    enum Speed{
        MILES_PER_SECOND(1862.871), MILES_PER_HOUR(6706335.6), FEET_PER_SECOND(9835958), FEET_PER_MINUTE(590157500),
        KILOMETRES_PER_SECOND(3998), METRES_PER_SECOND(2998000), KILOMETRES_PER_HOUR(107922800), METRES_PER_MINUTE(179880000),
        SHEEP_IN_VACUMN(100);

        double value;

        Speed(double d){
            this.value = d;
        }

    }

    public static Map<String,Double> caculateArea(double num, Area field){
        Map<String,Double> results = new HashMap<String,Double>(17);

        for(Area a : Area.values()){
            results.put(a.name(), num * (a.value / field.value));
        }

        return results;
    }

    public static Map<String,Double> calculateweight(double num, Weight field){
        Map<String,Double> results = new HashMap<String,Double>(13);

        for(Weight w : Weight.values()){
            results.put(w.name(), num * (w.value / field.value));
        }

        return results;
    }

    public static Map<String,Double> calculateVolume(double num, Volume field){
        Map<String,Double> results = new HashMap<String,Double>(13);

        for(Volume v : Volume.values()){
            results.put(v.name(), num * (v.value / field.value));
        }

        return results;
    }

    //HERE BE DRAGONS
    public static Map<String,Double> calculateTemp(double num, Temp field){
        Map<String,Double> results = new HashMap<String,Double>(3);

       if(field == Temp.CELSIUS){
           results.put(Temp.FAHRENHEIT.name(), (((9 / 5) * num) + 32));
           results.put(Temp.CELSIUS.name(), num);
           results.put(Temp.HILTON.name(), ((num * 0.1) - 2));
       }else if(field == Temp.FAHRENHEIT){
           results.put(Temp.FAHRENHEIT.name(), num);
           results.put(Temp.CELSIUS.name(), (((5/9)*(num - 32))));
           results.put(Temp.HILTON.name(), (((((5/9)*(num - 32))) * 0.1) -2));
       }else{
           results.put(Temp.FAHRENHEIT.name(), (((9/5)*((num + 2)/0.1)) + 32));
           results.put(Temp.CELSIUS.name(), ((num + 2)/0.1));
           results.put(Temp.HILTON.name(), num);
       }

        return results;
    }

    public static Map<String,Double> calculateSpeed(double num, Speed field){
        Map<String,Double> results = new HashMap<String,Double>(9);

        for(Speed s : Speed.values()){
            results.put(s.name(), num * (s.value / field.value));
        }

        return results;
    }

    public static Map<String,Double> calculateLength(double num, Length field){
        Map<String,Double> results = new HashMap<String,Double>(13);

        for(Length l : Length.values()){
            results.put(l.name(), num * (l.value / field.value));
        }

        return results;
    }

    public static Map<String,Double> calculateForce(double num, Force field){
        Map<String,Double> results = new HashMap<String,Double>(6);

        for(Force f : Force.values()){
            results.put(f.name(), num * (f.value / field.value));
        }

        return results;
    }

}
