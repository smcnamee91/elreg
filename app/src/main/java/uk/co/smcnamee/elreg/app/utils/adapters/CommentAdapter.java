/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app.utils.adapters;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import elreg.smcnamee.co.uk.secureapi.model.Forum;
import uk.co.smcnamee.elreg.app.R;

/**
 * Created by a543097 on 30/07/2015.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    Forum forum;

    public CommentAdapter(Forum forum) {
        this.forum = forum;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //create view and viewholder
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_forum, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.handle.setText(forum.getPosts().get(position).getHandle());
        holder.time.setText(forum.getPosts().get(position).getTimeSincePost());
        holder.title.setText(Html.fromHtml(forum.getPosts().get(position).getHeadline()));
        holder.content.setText(Html.fromHtml(forum.getPosts().get(position).getBody()));
    }

    @Override
    public int getItemCount() {
        return forum.getPosts().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView handle;
        public TextView time;
        public TextView title;
        public TextView content;

        public ViewHolder(View itemView) {
            super(itemView);
            handle = ((TextView)itemView.findViewById(R.id.post_handle));
            time = ((TextView)itemView.findViewById(R.id.post_time));
            title = ((TextView)itemView.findViewById(R.id.post_title));
            content = ((TextView)itemView.findViewById(R.id.post_content));

            scaleFonts(itemView.getContext(), itemView,
                    PreferenceManager.getDefaultSharedPreferences(itemView.getContext()).getFloat("basic_slider", 1));
        }

        private void scaleFonts(final Context context, final View v, float scale) {
            try {
                if (v instanceof ViewGroup) {
                    ViewGroup vg = (ViewGroup) v;
                    for (int i = 0; i < vg.getChildCount(); i++) {
                        View child = vg.getChildAt(i);
                        scaleFonts(context, child, scale);
                    }
                } else if (v instanceof TextView) {
                    ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, ((TextView) v).getTextSize()
                            * (scale));
                }
            } catch (Exception e) {
            }
        }
    }
}
