package uk.co.smcnamee.elreg.app.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import elreg.smcnamee.co.uk.secureapi.model.Article;
import elreg.smcnamee.co.uk.secureapi.model.Forum;
import elreg.smcnamee.co.uk.secureapi.model.Story;
import elreg.smcnamee.co.uk.secureapi.wrapper.SecureApi;
import uk.co.smcnamee.elreg.app.R;
import uk.co.smcnamee.elreg.app.activities.ArticleActivity;
import uk.co.smcnamee.elreg.app.activities.ArticleSwipeActivity;
import uk.co.smcnamee.elreg.app.activities.ForumActivity;
import uk.co.smcnamee.elreg.app.activities.MainActivity;
import uk.co.smcnamee.elreg.app.utils.TagHandler;
import uk.co.smcnamee.elreg.app.utils.URLImageParser;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ArticleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleFragment extends Fragment implements SecureApi.ApiArticleCallbacks, SecureApi.ApiForumCallbacks, SharedPreferences.OnSharedPreferenceChangeListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_POSITION = "param1";
    private static final String ARG_STORY = "param2";
    private static final String ARG_FORUM = "param3";

    // TODO: Rename and change types of parameters
    private int mParam1;
    private Story story;
    private View root;
    private boolean buttonPressed;
    private Article article;
    SecureApi elRegApi;
    private Forum forum;
    private float headline_size;
    private float caption_size;
    private float body1_size;
    private float body2_size;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ArticleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ArticleFragment newInstance(int pos, Story story) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, pos);
        args.putParcelable(ARG_STORY,story);
        fragment.setArguments(args);
        return fragment;
    }

    public ArticleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_POSITION);
            story = getArguments().getParcelable(ARG_STORY);
        }else if(savedInstanceState != null){
            story = savedInstanceState.getParcelable(ARG_STORY);
            forum = savedInstanceState.getParcelable(ARG_FORUM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_article, container, false);

        if(savedInstanceState != null){
            story = savedInstanceState.getParcelable(ARG_STORY);
            forum = savedInstanceState.getParcelable(ARG_FORUM);
        }

        elRegApi = SecureApi.getInstance(container.getContext(), getString(R.string.api_username), getString(R.string.api_key));

        loadArticle(container.getContext(), story);

        PreferenceManager.getDefaultSharedPreferences(getActivity())
                .registerOnSharedPreferenceChangeListener(this);

        headline_size = getResources().getDimension(R.dimen.abc_text_size_headline_material);
        caption_size = getResources().getDimension(R.dimen.abc_text_size_caption_material);
        body1_size = getResources().getDimension(R.dimen.abc_text_size_body_1_material);
        body2_size = getResources().getDimension(R.dimen.abc_text_size_body_2_material);

        scaleFonts(getActivity(), root.findViewById(R.id.scrollView),
                PreferenceManager.getDefaultSharedPreferences(getActivity()).getFloat("basic_slider", 1));

        return root;
    }

    private void loadArticle(Context ctx, final Story story){
        File[] cachedStory = ctx.getCacheDir().listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                try {
                    return pathname.getName().equals(story.getId() + ".json");
                }catch (NullPointerException e){
                    return false;
                }
            }
        });

        if(cachedStory != null){
            try {
                Log.i("cacheHit", "Loading from cache: " + story.getId());
                FileInputStream fis = new FileInputStream(ctx.getCacheDir() + "/" + story.getId() + ".json");
                BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                onArticleLoaded(new Article(new JSONObject(sb.toString())));
            } catch (JSONException | FileNotFoundException e) {
                if(isConnectedToNetwork()){
                    elRegApi.getArticle(story);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            elRegApi.getArticle(story);
        }
    }

    public void onArticleLoaded(Article article) {
        final Story st = this.story;
        final SecureApi.ApiForumCallbacks callbacks = this;

        TextView textView = (TextView) root.findViewById(R.id.story_title);
        textView.setText(article.getHeadline());


        textView = (TextView) root.findViewById(R.id.story_standfirst);
        textView.setText(Html.fromHtml(article.getIntroduction(), null, new TagHandler()));

        textView = (TextView) root.findViewById(R.id.story_writer);
        textView.setText(Html.fromHtml(article.getByline()));

        final Button button = (Button) root.findViewById(R.id.story_button);
        button.setText(article.getForumPosts() + " Comments");
        if(isConnectedToNetwork()){
            elRegApi.getArticleForum(this.story, this);
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnectedToNetwork()) {
                    buttonPressed = true;
                    elRegApi.getArticleForum(st, callbacks);
                } else {
                    Toast.makeText(root.getContext(), "No internet connection available, " +
                            "unable to load comments", Toast.LENGTH_SHORT).show();
                }
            }
        });

        URLImageParser p = new URLImageParser(root.findViewById(R.id.story_body), root.getContext());
        textView = (TextView) root.findViewById(R.id.story_body);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        Spanned s = Html.fromHtml(article.getArticleBody(), p, new TagHandler());

        textView.setText(s);

        textView.invalidate();
        textView.setTransformationMethod(null);

        textView.setText(textView.getText());

        this.article = article;
//        Toast.makeText(this, "finished spans", Toast.LENGTH_SHORT).show();

    }

    private boolean isConnectedToNetwork(){
        ConnectivityManager cm = (ConnectivityManager) root.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return  (activeNetwork != null) && (activeNetwork.isConnectedOrConnecting());
    }

    // Call to update the share intent
    public void getShareIntent() {
        if(((ArticleSwipeActivity)getActivity()).getmShareActionProvider() != null){
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");

            shareIntent.putExtra(Intent.EXTRA_SUBJECT, this.article.getHeadline());
            shareIntent.putExtra(Intent.EXTRA_TEXT,( "http://m.theregister.co.uk" + story.getUrl()));
            ((ArticleSwipeActivity)getActivity()).getmShareActionProvider().setShareIntent(shareIntent);
        }

    }


    @Override
    public void onForumPostsLoaded(Forum forum) {
        this.forum = forum;
        Button button = (Button) root.findViewById(R.id.story_button);
        button.setText(forum.getPosts().size() + " Comments");
        if (buttonPressed) {
            if (forum.getPosts().size() > 0) {
                Intent intent = new Intent(getActivity(), ForumActivity.class);
                intent.putExtra("FORUM", forum);
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), "No comments to load", Toast.LENGTH_SHORT).show();
            }
            buttonPressed = false;
        }
    }

    private void scaleFonts(final Context context, final View v, float scale) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    scaleFonts(context, child, scale);
                }
            } else if (v instanceof TextView) {
                switch (v.getId()){
                    case R.id.story_title:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, headline_size * (scale));
                        break;
                    case R.id.story_standfirst:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, body2_size * (scale));
                        break;
                    case R.id.story_writer:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, caption_size * (scale));
                        break;
                    case R.id.story_body:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, body1_size * (scale));
                        break;
                }

            }
        } catch (Exception e) {
            Log.e("ScaleFont", e.getMessage());
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("basic_slider")){
            scaleFonts(getActivity(), root.findViewById(R.id.scrollView),sharedPreferences.getFloat(key,1));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        icicle.putParcelable(ARG_STORY,story);
        if(forum != null){
            icicle.putParcelable(ARG_FORUM,forum);
        }
    }
}
