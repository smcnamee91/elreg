package uk.co.smcnamee.elreg.app.fragments.helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import java.util.List;

import elreg.smcnamee.co.uk.secureapi.model.Story;
import uk.co.smcnamee.elreg.app.fragments.ArticleFragment;

/**
 * Created by Simon on 13/08/2015.
 */
public class ArticlePagerAdapter extends FragmentStatePagerAdapter {

    List<Story> stories;
    private static int NUM_ITEMS = 5;

    public ArticlePagerAdapter(FragmentManager supportFragmentManager, List<Story> stories) {
        super(supportFragmentManager);
        this.stories = stories;
        NUM_ITEMS = stories.size();
    }

    @Override
    public Fragment getItem(int i) {
        return ArticleFragment.newInstance(i, stories.get(i));
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

}
