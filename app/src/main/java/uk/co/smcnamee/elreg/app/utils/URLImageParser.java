/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import uk.co.smcnamee.elreg.app.activities.ArticleActivity;
import uk.co.smcnamee.elreg.app.views.URLDrawable;

/**
 * Created by a543097 on 23/06/2015.
 */
public class URLImageParser implements Html.ImageGetter {
    Context c;
    View container;

    /***
     * Construct the URLImageParser which will execute AsyncTask and refresh the container
     * @param t
     * @param c
     */
    public URLImageParser(View t, Context c) {
        this.c = c;
        this.container = t;
    }

    public Drawable getDrawable(String source) {
        URLDrawable urlDrawable = new URLDrawable();

        // get the actual source
        ImageGetterAsyncTask asyncTask =
                new ImageGetterAsyncTask( urlDrawable);

        asyncTask.execute(source);

        // return reference to URLDrawable where I will change with actual image from
        // the src tag
        return urlDrawable;
    }

    public class ImageGetterAsyncTask extends AsyncTask<String, Void, Drawable> {
        URLDrawable urlDrawable;

        public ImageGetterAsyncTask(URLDrawable d) {
            this.urlDrawable = d;
        }

        @Override
        protected Drawable doInBackground(String... params) {
            String source = params[0];
            return fetchDrawable(source);
        }

        @Override
        protected void onPostExecute(Drawable result) {

            if(result == null)return;

            // set the correct bound according to the result from HTTP call
            float multiplier = ArticleActivity.ARTICLE_WIDTH / (float)result.getIntrinsicWidth();

            int width = (int)(result.getIntrinsicWidth() * multiplier);
            int height = (int)(result.getIntrinsicHeight() * multiplier);

            result.setBounds(0, 0, width, height);

            // change the reference of the current drawable to the result
            // from the HTTP call
            urlDrawable.setDrawable(result);

            // redraw the image by invalidating the container
            URLImageParser.this.container.invalidate();

//                ((TextView)URLImageParser.this.container).setHeight((URLImageParser.this.container.getHeight()
//                        + result.getIntrinsicHeight()));

            ((TextView)URLImageParser.this.container).setText(((TextView)URLImageParser.this.container).getText());
            //((LinearLayout)URLImageParser.this.container.getParent()).getLayoutParams().height += LinearLayout.LayoutParams.WRAP_CONTENT;

        }

        /***
         * Get the Drawable from URL
         * @param urlString
         * @return
         ***/
        public Drawable fetchDrawable(String urlString) {
            try {
                InputStream is = fetch(urlString);

                Drawable drawable = Drawable.createFromStream(is, "src");
                float multiplier = ArticleActivity.ARTICLE_WIDTH / (float)drawable.getIntrinsicWidth();

                int width = (int)(drawable.getIntrinsicWidth() * multiplier);
                int height = (int)(drawable.getIntrinsicHeight() * multiplier);

                urlDrawable.setBounds(0, 0, width, height);

                return drawable;
            } catch (Exception e) {
                return null;
            }
        }

        private InputStream fetch(String urlString) throws MalformedURLException, IOException {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet request = new HttpGet(urlString);
            HttpResponse response = httpClient.execute(request);
            return response.getEntity().getContent();
        }
    }
}
