/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import uk.co.smcnamee.elreg.app.R;

/**
 * Created by a543097 on 18/05/2015.
 */
public class SliderPreference extends DialogPreference {

    protected final static int SEEKBAR_RESOLUTION = 20;

    protected float mValue;
    protected int mSeekBarValue;
    protected CharSequence[] mSummaries;

    float title_size;
    float body2_size;
    float body1_size;

    /**
     * @param context
     * @param attrs
     */
    public SliderPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context, attrs);
    }

    /**
     * @param context
     * @param attrs
     * @param defStyle
     */
    public SliderPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup(context, attrs);
    }

    private void setup(Context context, AttributeSet attrs) {
        setDialogLayoutResource(R.layout.slider_preference_dialog);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SliderPreference);
        try {
            setSummary(a.getTextArray(R.styleable.SliderPreference_android_summary));
        } catch (Exception e) {
            // Do nothing
        }
        a.recycle();
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getFloat(index, 0);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setValue(restoreValue ? (getPersistedFloat(mValue)) : 1f);
    }

    @Override
    public CharSequence getSummary() {
        if (mSummaries != null && mSummaries.length > 0) {
            int index = (int) (mValue * mSummaries.length);
            index = Math.min(index, mSummaries.length - 1);
            return mSummaries[index];
        } else {
            return super.getSummary();
        }
    }

    private void scaleFonts(final Context context, final View v) {
        float scale = ((float)mSeekBarValue / SEEKBAR_RESOLUTION);
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    scaleFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, ((TextView) v).getTextSize()
                        * ((scale) + 0.7f));
            }
        } catch (Exception e) {
        }
    }

    public void setSummary(CharSequence[] summaries) {
        mSummaries = summaries;
    }

    public float getValue() {
        return mValue;
    }

    public void setValue(float value) {
        value = Math.max(0.7f, Math.min(value, 1.7f)); // clamp to [0, 1]
        if (shouldPersist()) {
            persistFloat(value);
        }
        if (value != mValue) {
            mValue = value;
            notifyChanged();
        }
    }

    @Override
    protected View onCreateDialogView() {
        mSeekBarValue = (int) ((mValue-0.7f) * SEEKBAR_RESOLUTION);
        final View view = super.onCreateDialogView();
        SeekBar seekbar = (SeekBar) view.findViewById(R.id.slider_preference_seekbar);
        seekbar.setMax(SEEKBAR_RESOLUTION);
        seekbar.setProgress(mSeekBarValue);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    SliderPreference.this.mSeekBarValue = progress;

                    TextView textView = (TextView) ((View)seekBar.getParent()).findViewById(R.id.title_example);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, title_size
                            * (((float)mSeekBarValue / SEEKBAR_RESOLUTION)+ 0.7f));

                    textView = (TextView) ((View)seekBar.getParent()).findViewById(R.id.body2_example);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, body2_size
                            * (((float)mSeekBarValue / SEEKBAR_RESOLUTION)+ 0.7f));


                    textView = (TextView) ((View)seekBar.getParent()).findViewById(R.id.body1_example);
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, body1_size
                            * (((float)mSeekBarValue / SEEKBAR_RESOLUTION)+ 0.7f));
                }

                //Toast.makeText(getContext() ,"" + ((float)mSeekBarValue / SEEKBAR_RESOLUTION + 0.7f),Toast.LENGTH_SHORT).show();
            }
        });

        TextView textView = (TextView) ((View)seekbar.getParent()).findViewById(R.id.title_example);
        title_size = textView.getTextSize();

        textView = (TextView) ((View)seekbar.getParent()).findViewById(R.id.body2_example);
        body2_size = textView.getTextSize();

        textView = (TextView) ((View)seekbar.getParent()).findViewById(R.id.body1_example);
        body1_size = textView.getTextSize();


        scaleFonts(getContext(), view.findViewById(R.id.slider_preference_layout));

        return view;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        float newValue = (((float)mSeekBarValue / SEEKBAR_RESOLUTION)+ 0.7f);
        if (positiveResult && callChangeListener(newValue)) {
            setValue(newValue);
        }
        super.onDialogClosed(positiveResult);
    }

    // TODO: Save and restore preference state.
}