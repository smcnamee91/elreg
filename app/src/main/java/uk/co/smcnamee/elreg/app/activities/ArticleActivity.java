/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package uk.co.smcnamee.elreg.app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.text.style.QuoteSpan;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import elreg.smcnamee.co.uk.secureapi.model.Article;
import elreg.smcnamee.co.uk.secureapi.model.Forum;
import elreg.smcnamee.co.uk.secureapi.model.Story;
import elreg.smcnamee.co.uk.secureapi.wrapper.SecureApi;
import uk.co.smcnamee.elreg.app.R;
import uk.co.smcnamee.elreg.app.settings.SettingsActivity;
import uk.co.smcnamee.elreg.app.utils.TagHandler;
import uk.co.smcnamee.elreg.app.utils.URLImageParser;
import uk.co.smcnamee.elreg.app.views.TweetSpan;
import uk.co.smcnamee.elreg.app.views.URLDrawable;


public class ArticleActivity extends AppCompatActivity implements SecureApi.ApiArticleCallbacks, SecureApi.ApiForumCallbacks, SharedPreferences.OnSharedPreferenceChangeListener{

    public static float ARTICLE_WIDTH;
    private Toolbar mToolbar;
    private SecureApi elRegApi;

    private Forum forum;
    private Story story;

    private float lastScaleValue;
    private boolean buttonPressed = false;

    private Article article;

    float headline_size;
    float caption_size;
    float body2_size;
    float body1_size;

    int offset = 0;
    private ShareActionProvider mShareActionProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_art);

        if(savedInstanceState != null){
            story = savedInstanceState.getParcelable("STORY");
            forum = savedInstanceState.getParcelable("FORUM");
        }else{
            story = getIntent().getParcelableExtra("STORY");
        }


        mToolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ARTICLE_WIDTH = ((float)getResources().getDisplayMetrics().widthPixels - dpToPx(32));

        elRegApi = SecureApi.getInstance(this,
                getResources().getString(R.string.api_username),
                getResources().getString(R.string.api_key));

        loadArticle();

        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);

        headline_size = getResources().getDimension(R.dimen.abc_text_size_headline_material);
        caption_size = getResources().getDimension(R.dimen.abc_text_size_caption_material);
        body1_size = getResources().getDimension(R.dimen.abc_text_size_body_1_material);
        body2_size = getResources().getDimension(R.dimen.abc_text_size_body_2_material);


        scaleFonts(this, findViewById(R.id.root_view),
                PreferenceManager.getDefaultSharedPreferences(this).getFloat("basic_slider",1));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_art, menu);

        MenuItem shareItem = menu.findItem(R.id.menu_item_share);
        mShareActionProvider = (ShareActionProvider)MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(new Intent(Intent.ACTION_SEND));
        getShareIntent();

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent i;

        switch (id){
            case R.id.action_about:
                i = new Intent(this, AboutActivity.class);
                startActivity(i);
                break;
            case R.id.action_settings:
                i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    // Call to update the share intent
    private void getShareIntent() {
        if(mShareActionProvider != null){
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");

            shareIntent.putExtra(Intent.EXTRA_SUBJECT, this.article.getHeadline());
            shareIntent.putExtra(Intent.EXTRA_TEXT,( "http://m.theregister.co.uk" + story.getUrl()));
            mShareActionProvider.setShareIntent(shareIntent);
        }

    }

    private void loadArticle(){
        File[] cachedStory = getCacheDir().listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                try {
                    return pathname.getName().equals(story.getId() + ".json");
                }catch (NullPointerException e){
                    return false;
                }
            }
        });

        if(cachedStory != null){
            try {
                Log.i("cacheHit", "Loading from cache: " + story.getId());
                FileInputStream fis = new FileInputStream(getCacheDir() + "/" + story.getId() + ".json");
                BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                onArticleLoaded(new Article(new JSONObject(sb.toString())));
            } catch (JSONException | FileNotFoundException e) {
                if(isConnectedToNetwork()){
                    elRegApi.getArticle(story);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            elRegApi.getArticle(story);
        }
    }

    @Override
    protected void onResume() {
        try{
            super.onResume();
        } catch(NullPointerException npe){
            finish();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        icicle.putParcelable("STORY", story);
        if(forum != null){
            icicle.putParcelable("FORUM",forum);
        }
    }

    @Override
    public void onArticleLoaded(Article article) {
        TextView textView = (TextView) findViewById(R.id.story_title);
        textView.setText(article.getHeadline());


        textView = (TextView) findViewById(R.id.story_standfirst);
        textView.setText(Html.fromHtml(article.getIntroduction(), null, new TagHandler()));

        textView = (TextView) findViewById(R.id.story_writer);
        textView.setText(Html.fromHtml(article.getByline()));

        final Button button = (Button) findViewById(R.id.story_button);
        button.setText(article.getForumPosts() + " Comments");
        if(isConnectedToNetwork()){
            elRegApi.getArticleForum(story);
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isConnectedToNetwork()){
                    buttonPressed = true;
                    elRegApi.getArticleForum(story);
                } else{
                    Toast.makeText(ArticleActivity.this, "No internet connection available, " +
                            "unable to load comments", Toast.LENGTH_SHORT).show();
                }
            }
        });

        URLImageParser p = new URLImageParser(findViewById(R.id.story_body), ArticleActivity.this);
        textView = (TextView) findViewById(R.id.story_body);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

        Spanned s = replaceQuoteSpans(
                new SpannableString(Html.fromHtml(article.getArticleBody(), p, new TagHandler())));

        textView.setText(s);

        textView.invalidate();
        textView.setTransformationMethod(null);

        textView.setText(textView.getText());

        this.article = article;
        getShareIntent();

//        Toast.makeText(this, "finished spans", Toast.LENGTH_SHORT).show();

    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    public void onForumPostsLoaded(Forum forum) {
        this.forum = forum;
        Button button = (Button) findViewById(R.id.story_button);
        button.setText(forum.getPosts().size() + " Comments");
        if(buttonPressed){
            if(forum.getPosts().size() > 0){
                Intent intent = new Intent(this, ForumActivity.class);
                intent.putExtra("FORUM",forum);
                startActivity(intent);
            }else{
                Toast.makeText(ArticleActivity.this,"No comments to load",Toast.LENGTH_SHORT).show();
            }
            buttonPressed = false;
        }

    }

    public Forum getForum() {
        return forum;
    }

    private boolean isConnectedToNetwork(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return  (activeNetwork != null) && (activeNetwork.isConnectedOrConnecting());
    }

    private String hashIt(String s) {
        return new String(Hex.encodeHex(DigestUtils.sha1(s)));
    }

    private void scaleFonts(final Context context, final View v, float scale) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    scaleFonts(context, child, scale);
                }
            } else if (v instanceof TextView) {
                switch (v.getId()){
                    case R.id.story_title:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, headline_size * (scale));
                        break;
                    case R.id.story_standfirst:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, body2_size * (scale));
                        break;
                    case R.id.story_writer:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, caption_size * (scale));
                        break;
                    case R.id.story_body:
                        ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_PX, body1_size * (scale));
                        break;
                }

            }
        } catch (Exception e) {
            Log.e("ScaleFont", e.getMessage());
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("basic_slider")){
            scaleFonts(this, findViewById(R.id.scrollView),sharedPreferences.getFloat(key,1));
        }
    }

    private Spanned replaceQuoteSpans(Spannable span) {
        SpannableStringBuilder spannable = new SpannableStringBuilder(span);

        QuoteSpan[] quoteSpans = spannable.getSpans(0, spannable.length(), QuoteSpan.class);
        for (QuoteSpan quoteSpan : quoteSpans) {
            int start = spannable.getSpanStart(quoteSpan);
            int end = spannable.getSpanEnd(quoteSpan);
            offset = 0;

            URLSpan[] urlSpans = spannable.getSpans(start,end,URLSpan.class);

            for(URLSpan url : urlSpans){
                if(url.getURL().toLowerCase().contains("twitter.com/") && url.getURL().toLowerCase().contains("/status/")) {

                    long tweetId = Long.parseLong(url.getURL().split("/")[url.getURL().split("/").length - 1]);

                    spannable.removeSpan(quoteSpan);
                    spannable.removeSpan(url);

                    new TweetSpan(this,start,end).getTweetImage(tweetId);

                }

            }

        }
        return spannable;
    }

    public void updateSpan(URLDrawable tweetImage, int startSpan, int endSpan){

        int start = startSpan -offset;
        int end = endSpan - offset;

        final TextView textView  = (TextView) findViewById(R.id.story_body);
        SpannableStringBuilder ss = new SpannableStringBuilder(textView.getText());

        Drawable d = tweetImage.getDrawable();
        d.setBounds(0, 0, (int) (d.getIntrinsicWidth() * 3), (int) (d.getIntrinsicHeight() * 3));

        ImageSpan span = new ImageSpan(d , ImageSpan.ALIGN_BASELINE);

        ss.setSpan(span, start, start + (1), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        offset += (end - (start + 1));

        ss.replace(start+1, end, "");
//        ss.replace(end,end,"\n\n");


        SpannedString s = SpannedString.valueOf(ss);

        textView.invalidate();
        textView.setText(ss);
        textView.invalidate();
        textView.requestLayout();
    }


}
