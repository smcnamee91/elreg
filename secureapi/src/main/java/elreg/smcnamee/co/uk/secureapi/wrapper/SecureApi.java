/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package elreg.smcnamee.co.uk.secureapi.wrapper;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import elreg.smcnamee.co.uk.secureapi.model.Article;
import elreg.smcnamee.co.uk.secureapi.model.Forum;
import elreg.smcnamee.co.uk.secureapi.model.Story;

/**
 * Created by Simon on 30/01/2015.
 */
public class SecureApi {
    
    private static SecureApi secureApi;
    
    private String username;
    private String key;
    private Context context;


    private SecureApi(Context context, String username, String key){
        this.context = context;
        this.username = username;
        this.key = key;
    }

    public static SecureApi getInstance(Context context, String username, String key){

        if(secureApi != null && (secureApi.context.equals(context)
                && secureApi.username.equals(username)
                && secureApi.key.equals(key))){

            return secureApi;
        } else{

            secureApi = new SecureApi(context, username, key);
            ApiVolley.init(context);

            return secureApi;
        }
        
    }

    public void getSectionStories(final int numberToLoad, final String sectionUrl){

        RequestQueue requestQueue = ApiVolley.getRequestQueue();

        JsonObjectRequest sectionRequest = new JsonObjectRequest(Method.GET,
                sectionUrl,
                "",
                createSectionSuccessListener(numberToLoad, sectionUrl),
                createErrorListener()){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>(super.getHeaders());
                return headers;
            }

            @Override
            public Priority getPriority() {
                return Priority.NORMAL;
            }
        };

        requestQueue.add(sectionRequest);

    }


    public void getAllSectionStories(final int numberPerSection, final String[] sectionsArray){
        RequestQueue requestQueue = ApiVolley.getRequestQueue();

        JsonObjectRequest allSectionRequest = new JsonObjectRequest(Method.GET,
                "http://m.theregister.co.uk/batch/recent/" + numberPerSection + "?f=json",
                "",
                createAllSectionSuccessListener(sectionsArray),
                createErrorListener()){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>(super.getHeaders());
                return headers;
            }

            @Override
            public Priority getPriority() {
                return Priority.LOW;
            }
        };

        allSectionRequest.setShouldCache(false);

        requestQueue.add(allSectionRequest);
    }

     /**
     * * Load the article content for a specified story
     * *
     * @param story The story to get the article for
     */
    public void getArticle(final Story story){
        RequestQueue requestQueue = ApiVolley.getRequestQueue();

        JsonObjectRequest articleRequest = new JsonObjectRequest(Method.GET,
                "http://m.theregister.co.uk" + story.getUrl() + "?f=json",
                "",
                createArticleSuccessListener(),
                createErrorListener()){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>(super.getHeaders());
                headers.put("X-Reg-JSON-API", (System.currentTimeMillis()/1000) + "-" + username + "-" +
                        hashIt((System.currentTimeMillis()/1000) + "-" + key + "-" + story.getSecret()));

                return headers;
            }

            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };

        requestQueue.add(articleRequest);
    }



    public void getAllArticles(HashMap<String, List<Story>> stories){
        RequestQueue requestQueue = ApiVolley.getRequestQueue();

        for(List<Story> storyList : stories.values()) {
            for(Story story : storyList){

                final Story s = story;

                JsonObjectRequest cacheArticleRequest = new JsonObjectRequest(Method.GET,
                        "http://m.theregister.co.uk" + story.getUrl() + "?f=json",
                        "",
                        createAllArticleSuccessListener(),
                        createErrorListener()){

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>(super.getHeaders());
                        headers.put("X-Reg-JSON-API", (System.currentTimeMillis()/1000) + "-" + username + "-" +
                                hashIt((System.currentTimeMillis()/1000) + "-" + key + "-" + s.getSecret()));
                        return headers;
                    }

                    @Override
                    public Priority getPriority() {
                        return Priority.LOW;
                    }
                };;


                requestQueue.add(cacheArticleRequest);
            }

        }


    }

    /**
     * * Load the article forum for a specified story
     * *
     * @param story The story to get the article for
     * @return An object representing the article requested
     */
    public void getArticleForum(final Story story){
        RequestQueue requestQueue = ApiVolley.getRequestQueue();

        JsonObjectRequest articleForumRequest = new JsonObjectRequest(Method.GET,
                "http://m.forums.theregister.co.uk/forum/1" + story.getUrl() + "?f=json&s=a",
                "",
                createArticleForumSuccessListener(story),
                createErrorListener()){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>(super.getHeaders());
                headers.put("X-Reg-JSON-API", (System.currentTimeMillis()/1000) + "-" + username + "-" +
                        hashIt((System.currentTimeMillis()/1000) + "-" + key + "-" + story.getSecret()));

                return headers;
            }

            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };

        requestQueue.add(articleForumRequest);
    }

    /**
     * * Load the article forum for a specified story
     * *
     * @param story The story to get the article for
     * @return An object representing the article requested
     */
    public void getArticleForum(final Story story, SecureApi.ApiForumCallbacks callbacks){
        RequestQueue requestQueue = ApiVolley.getRequestQueue();

        JsonObjectRequest articleForumRequest = new JsonObjectRequest(Method.GET,
                "http://m.forums.theregister.co.uk/forum/1" + story.getUrl() + "?f=json&s=a",
                "",
                createArticleForumSuccessListener(story, callbacks),
                createErrorListener()){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>(super.getHeaders());
                headers.put("X-Reg-JSON-API", (System.currentTimeMillis()/1000) + "-" + username + "-" +
                        hashIt((System.currentTimeMillis()/1000) + "-" + key + "-" + story.getSecret()));

                return headers;
            }

            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };

        requestQueue.add(articleForumRequest);
    }

    private List<JSONObject> parseBulkJsonSections(JSONObject jsonObject, String[] sections){
        List<JSONObject> result = new ArrayList<>();

        try{
            for(String path : sections ){
                result.add(jsonObject.getJSONObject(path));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }

    private String hashIt(String s) {
        return new String(Hex.encodeHex(DigestUtils.sha512(s)));
    }

    private void cacheImages(String body) {
    }


    private Response.Listener<JSONObject> createSectionSuccessListener(final int num, final String key) {
        return new Response.Listener<JSONObject>() {

            String resultKey = key;
            int numberToLoad = num;

            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray stories = response.getJSONArray("stories");

                    List<Story> result = new ArrayList<Story>(stories.length());

                    for(int i = 0; i < numberToLoad; i++){
                        result.add(new Story(stories.getJSONObject(i)));
                    }

                    ((ApiSectionCallbacks)context).onSectionLoaded(resultKey, result);

                } catch (JSONException e) {
                    Log.e("JSONException", "Error parsing section", e);
                }
            }
        };
    }



    private Response.Listener<JSONObject> createAllSectionSuccessListener(final String[] sectionsArray) {
        return new Response.Listener<JSONObject>() {

            String[] sectionsA = sectionsArray;

            @Override
            public void onResponse(JSONObject response) {
                try {
                    List<JSONObject> sections = parseBulkJsonSections(response.getJSONObject("sections"), sectionsA);

                    HashMap<String, List<Story>> result = new HashMap<>(sections.size());

                    for(JSONObject jO : sections) {

                        JSONArray stories = jO.getJSONArray("stories");

                        List<Story> storiesList = new ArrayList<Story>(stories.length());

                        for (int j = 0; j < stories.length(); j++) {
                            storiesList.add(new Story(stories.getJSONObject(j)));
                        }

                        result.put(jO.getString("category_path"), storiesList);

                        ((ApiSectionCallbacks) context).onGetAllSectionsComplete(result);

                        if(true){
                            getAllArticles(result);
                        }
                    }
                } catch (JSONException e) {
                    Log.e("JSONException", "Error parsing section", e);
                }
            }
        };
    }

    private Response.Listener<JSONObject> createArticleSuccessListener() {
        return new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                        ((ApiArticleCallbacks) context).onArticleLoaded(new Article(response));
                } catch (Exception e) {
                    Log.e("Exception", "Error parsing Article", e);
                }
            }
        };
    }

    private Response.Listener<JSONObject> createAllArticleSuccessListener() {
        return new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    File f = new File(context.getCacheDir() + "/" + response.getString("id") +".json");
                    f.createNewFile();

                    BufferedWriter bw = new BufferedWriter(new FileWriter(f));

                    bw.write(response.toString(4));

                    bw.flush();
                    bw.close();
                } catch (Exception e) {
                    Log.e("Exception", "Error caching Article", e);
                }
            }
        };
    }

    private Response.Listener<JSONObject> createArticleForumSuccessListener(final Story stry) {
        return new Response.Listener<JSONObject>() {

            Story story = stry;

            @Override
            public void onResponse(JSONObject response) {
                try {
                    ((ApiForumCallbacks) context).onForumPostsLoaded(new Forum(story, response));
                } catch (Exception e) {
                    Log.e("Exception", "Error parsing Article", e);
                }
            }
        };
    }

    private Response.Listener<JSONObject> createArticleForumSuccessListener(final Story stry, final ApiForumCallbacks callbacks) {
        return new Response.Listener<JSONObject>() {

            Story story = stry;

            @Override
            public void onResponse(JSONObject response) {
                try {
                    callbacks.onForumPostsLoaded(new Forum(story, response));
                } catch (Exception e) {
                    Log.e("Exception", "Error parsing Article", e);
                }
            }
        };
    }


    private Response.ErrorListener createErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               //// TODO: 18/06/2015 implement error handling
            }
        };
    }

    public static interface ApiSectionCallbacks{

        public void onSectionLoaded(String key, List<Story> section);
        public void onGetAllSectionsComplete(HashMap<String, List<Story>> sections);
    }

    public static interface ApiArticleCallbacks{

        public void onArticleLoaded(Article article);
    }

    public static interface ApiForumCallbacks{

        public void onForumPostsLoaded(Forum forum);
    }
}
