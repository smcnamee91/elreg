/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package elreg.smcnamee.co.uk.secureapi.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by a543097 on 10/02/2015.
 */
public class Forum implements Parcelable{

    private String headline;
    private String synopsis;

    private List<Post> posts;



    public Forum(Story story, JSONObject forum) {
        this.headline = story.getHeadline();
        this.synopsis = story.getSynopsis();

        try{
            JSONArray postObjects = forum.getJSONArray("posts");
            int numberOfPosts = postObjects.length();
            posts = new ArrayList<>(numberOfPosts);

            for(int i = 0; i < numberOfPosts; i++){
               if(postObjects.getJSONObject(i).getString("state").equals("live")){
                   posts.add(new Post(postObjects.getJSONObject(i)));
               }
            }
        }catch (Exception e){

        }
    }

    protected Forum(Parcel in) {
        headline = in.readString();
        synopsis = in.readString();
        posts = new ArrayList<>();
        in.readTypedList(posts, Post.CREATOR);
        System.out.println(posts);
    }

    public static final Creator<Forum> CREATOR = new Creator<Forum>() {
        @Override
        public Forum createFromParcel(Parcel in) {
            return new Forum(in);
        }

        @Override
        public Forum[] newArray(int size) {
            return new Forum[size];
        }
    };

    public List<Post> getPosts(){
        return posts;
    }

    public String getTitle() {
        return headline;
    }

    public String getSynopsis() {
        return synopsis;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(headline);
        dest.writeString(synopsis);
        dest.writeTypedList(posts);
    }


    public static final class Post implements Parcelable{

        String title;
        String body;
        String imageUrl;

        String handle;
        boolean staff;

        String acceptedTime;
        boolean live;


        public Post(JSONObject postData){

            try{
                if(!postData.isNull("identity")){
                    this.handle = postData.getJSONObject("identity").getString("handle");
                } else{
                    this.handle = "Anonymous Coward";
                }
                this.title = postData.getJSONObject("data").getString("title");
                this.body = postData.getJSONObject("data").getString("body");
                this.imageUrl = postData.getJSONObject("data").getString("image_slug");
                this.staff = postData.getJSONObject("data").getString("staff").equals("1");
                this.acceptedTime = postData.getString("accepted_at");
                this.live = postData.getString("state").equals("live");
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        protected Post(Parcel in) {
            title = in.readString();
            body = in.readString();
            imageUrl = in.readString();
            handle = in.readString();
            staff = in.readString().equals("true");
            acceptedTime = in.readString();
            live = in.readString().equals("true");
        }

        public static final Creator<Post> CREATOR = new Creator<Post>() {
            @Override
            public Post createFromParcel(Parcel in) {
                return new Post(in);
            }

            @Override
            public Post[] newArray(int size) {
                return new Post[size];
            }
        };

        public CharSequence getTimeSincePost(){
            long then = new Date((Long.parseLong(acceptedTime) * 1000)).getTime();
            long now = new Date().getTime();

            return DateUtils.getRelativeTimeSpanString(then, now, DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE);
        }

        public String getHeadline() {
            return title;
        }

        public String getBody() {
            return body;
        }

        public boolean isLive() {
            return live;
        }

        public String getHandle(){
            if(handle == null){
                return "Anonymous Coward";
            } else{
                return handle;
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(title);
            dest.writeString(body);
            dest.writeString(imageUrl);
            dest.writeString(handle);
            dest.writeString(staff + "");
            dest.writeString(acceptedTime);
            dest.writeString(live + "");
        }
    }

}
