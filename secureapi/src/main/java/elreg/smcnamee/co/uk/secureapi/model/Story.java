/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package elreg.smcnamee.co.uk.secureapi.model;

import android.text.format.DateUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Simon on 31/01/2015.
 */
public class Story implements Serializable{
    private String epoch;
    private String secret;
    private String forumPosts;
    private String headline;
    private String standfirst;
    private String synopsis;
    private String url;
    private String id;
    
    public Story(JSONObject jsonObject){
        
        try {
            
            epoch = jsonObject.getString("epoch");
            secret = jsonObject.getString("secret");
            forumPosts = jsonObject.getString("forum_public_posts");
            headline = jsonObject.getString("headline");
            standfirst = jsonObject.getString("standfirst");
            synopsis = jsonObject.getString("synopsis");
            url = jsonObject.getString("url");
            id = jsonObject.getString("id");
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getEpoch() {
        return epoch;
    }

    public CharSequence getTimeSincePost(){
        long then = new Date((Long.parseLong(epoch) * 1000)).getTime();
        long now = new Date().getTime();

        return DateUtils.getRelativeTimeSpanString(then, now, DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE);
    }

    public String getSecret() {
        return secret;
    }

    public String getUrl() {
        return url;
    }

    public String getHeadline() {
        return headline;
    }

    public String getStandfirst() {
        return standfirst;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public String getId() {
        return id;
    }

    public String getForumPosts() {
        return forumPosts;
    }
}
