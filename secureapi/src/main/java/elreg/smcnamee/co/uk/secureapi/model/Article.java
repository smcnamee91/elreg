/*
 *     Copyright (c) 2015.
 *
 *     This file is part of ElReg.
 *
 *     ElReg is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ElReg is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ElReg.  If not, see <http://www.gnu.org/licenses/>
 */

package elreg.smcnamee.co.uk.secureapi.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Simon on 31/01/2015.
 */
public class Article {

    private String id;
    private String headline;
    private String introduction;
    private String authorName;
    private String byline;
    private String category;
    private String categoryPath;
    private String forumPosts;
    private String articleBody;
    
    public Article(JSONObject jsonObject){
        
        try {
            
            this.id = jsonObject.getString("id");
            this.headline = jsonObject.getString("headline");
            this.introduction = jsonObject.getString("introduction");
            this.authorName = jsonObject.getString("author_name");
            this.byline = jsonObject.getString("byline");
            this.category = jsonObject.getString("category");
            this.categoryPath = jsonObject.getString("category_path");
            this.forumPosts = jsonObject.getString("forum_public_posts");
            this.articleBody = jsonObject.getString("body");
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public String getCategoryPath() {
        return categoryPath;
    }

    public String getArticleBody() {
        return articleBody;
    }

    public String getHeadline() {
        return headline;
    }

    public String getByline() {
        return byline;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getForumPosts() {
        return forumPosts;
    }
}
